package auction

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"gitlab.com/maxboychuk/course-project/internal/session"
	"gitlab.com/maxboychuk/course-project/internal/user"
	"gitlab.com/maxboychuk/course-project/pkg/errs"
	"gitlab.com/maxboychuk/course-project/pkg/lot"
	"gitlab.com/maxboychuk/course-project/pkg/tmplts"
	"gitlab.com/maxboychuk/course-project/pkg/ws"
)

func writeResponse(w http.ResponseWriter, db *sql.DB, lotID int64) error {
	l, err := lot.FindLotByID(db, lotID)
	if err != nil {
		return errors.Wrap(err, "can't find lot by ID")
	}

	resultJSON, err := json.MarshalIndent(l, "", "    ")
	if err != nil {
		return errors.Wrap(err, "can't marshal form")
	}

	w.WriteHeader(http.StatusOK) //status 200
	w.Header().Add("Content-Type", "application/json")
	_, _ = w.Write(resultJSON)

	return nil
}

func checkStatus(w http.ResponseWriter, l lot.Lot) {
	if l.Status == "finished" {
		fmt.Println("lot already deleted")
		errs.ErrorNotFound(w, "lot already deleted")
		return
	} else if l.Status == "active" {
		fmt.Println("can't delete lot, lot is active")
		errs.ErrorConflict(w, "can't delete lot, lot is active")
		return
	}
}

func convertToLotJSON(db *sql.DB, l lot.Lot) (lot.JSONLot, error) {
	creator, err := user.FindShortUserByID(db, l.CreatorID)
	if err != nil {
		fmt.Printf("can't find short user by ID: %s\n", err)
		return lot.JSONLot{}, errors.Wrap(err, "can't find user by ID")
	}

	buyer := user.ShortUser{}
	if l.BuyerID != 0 {
		buyer, err = user.FindShortUserByID(db, l.BuyerID)
		if err != nil {
			fmt.Printf("can't find short user by ID: %s\n", err)
			return lot.JSONLot{}, errors.Wrap(err, "can't find user by ID")
		}
	}

	lotJSON := lot.JSONLot{
		BaseLot: lot.BaseLot{
			LotID:        l.LotID,
			Title:        l.Title,
			Description:  l.Description,
			CurrentPrice: l.CurrentPrice,
			PriceStep:    l.PriceStep,
			Status:       l.Status,
			CreatedAt:    l.CreatedAt,
			UpdatedAt:    l.UpdatedAt,
			EndAt:        l.EndAt,
			DeletedAt:    l.DeletedAt,
		},
		Creator: creator,
		Buyer:   buyer,
	}

	return lotJSON, nil
}

func agingCheck(db *sql.DB, lotCh chan ws.LotWS) {
	const finished = "finished"
	for {
		lots, err := lot.GetLots(db)
		if err != nil {
			fmt.Printf("can't get lots: %s\n", err)
			time.Sleep(1 * time.Second)
			continue
		}

		for _, l := range lots {
			if (time.Now().After(l.EndAt)) && (l.Status != finished) {
				err = lot.SoftDeleteLot(db, l.LotID)
				if err != nil {
					fmt.Printf("can't soft delete lot: %s\n", err)
					continue
				}

				lotJSON, err := convertToLotJSON(db, l)
				if err != nil {
					fmt.Printf("can't convert to lot JSON: %s\n", err)
					continue
				}

				lotCh <- ws.LotWS{
					Req:      "lot_delete",
					LotIDStr: strconv.FormatInt(l.LotID, 10),
					Lot:      lotJSON,
				}

				fmt.Printf("lot finished\n")
			}
		}

		time.Sleep(1 * time.Second)
	}
}

func GetLots(db *sql.DB, templates map[string]*template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUserID, err := session.GetUserIDByCookie(r, db)
		if err != nil {
			errs.ErrorUnauthorized(w)
			return
		}

		status := r.URL.Query().Get("status")
		if (status != "created") && (status != "active") && (status != "finished") {
			status = ""
		}

		lots, err := lot.GetLots(db)
		if err != nil {
			fmt.Printf("can't get lots: %s\n", err)
			errs.ErrorConflict(w, "can't get lots")
			return
		}

		userAndLots := struct {
			UserID int64
			Lots   []lot.Lot
			Status string
		}{
			UserID: currentUserID,
			Lots:   lots,
			Status: status,
		}

		w.WriteHeader(http.StatusOK) // status 200
		err = tmplts.RenderTemplate(w, templates, "all_lots", userAndLots)
		if err != nil {
			fmt.Printf("can't render template: %s\n", err)
			errs.ErrorConflict(w, "can't render template")
			return
		}
	}
}

func GetLotsJSON(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		lots, err := lot.GetLots(db)
		if err != nil {
			fmt.Printf("can't get lots: %s\n", err)
			errs.ErrorConflict(w, "can't get lots")
			return
		}

		var lotJSON lot.JSONLot
		lotsJSON := make([]lot.JSONLot, len(lots))
		for i, l := range lots {
			lotJSON, err = convertToLotJSON(db, l)
			if err != nil {
				fmt.Printf("can't convert to lot JSON: %s\n", err)
				errs.ErrorConflict(w, "can't convert to lot JSON")
				return
			}
			lotsJSON[i] = lotJSON
		}

		result, err := json.MarshalIndent(lotsJSON, "", "    ")
		if err != nil {
			fmt.Printf("can't marshal json: %s\n", err)
			return
		}
		w.WriteHeader(http.StatusOK) // status 200
		_, _ = w.Write(result)
	}
}

func GetCreateLot(db *sql.DB, templates map[string]*template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		_, err := session.GetUserIDByCookie(r, db)
		if err != nil {
			errs.ErrorUnauthorized(w)
			return
		}

		err = tmplts.RenderTemplate(w, templates, "create_lot", nil)
		if err != nil {
			fmt.Printf("can't render template: %s\n", err)
			errs.ErrorConflict(w, "can't render template")
			return
		}
	}
}

func PostCreateLot(db *sql.DB, lotCh chan ws.LotWS) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUserID, err := session.GetUserIDByCookie(r, db)
		if err != nil {
			errs.ErrorUnauthorized(w)
			return
		}

		err = r.ParseForm()
		if err != nil {
			fmt.Printf("can't parse form in create lot: %s\n", err)
			errs.ErrorConflict(w, "can't parse form")
			return
		}

		lotID, err := lot.CreateLot(db, r.Form, currentUserID, time.Now())
		if err != nil {
			fmt.Printf("can't create lot: %s\n", err)
			errs.ErrorBadRequest(w, "can't create lot")
			return
		}

		l, err := lot.FindLotByID(db, lotID)
		if err != nil {
			fmt.Printf("can't find lot by ID: %s\n", err)
			errs.ErrorConflict(w, "can't find lot by ID")
			return
		}

		lotJSON, err := convertToLotJSON(db, l)
		if err != nil {
			fmt.Printf("can't convert to lot JSON: %s\n", err)
			errs.ErrorConflict(w, "can't convert to lot JSON")
			return
		}

		lotCh <- ws.LotWS{
			Req:      "lot_create",
			LotIDStr: strconv.FormatInt(lotID, 10),
			Lot:      lotJSON,
		}

		if err = writeResponse(w, db, lotID); err != nil {
			fmt.Printf("can't write response: %s\n", err)
			errs.ErrorConflict(w, "can't write response")
		}

		/*l, err = lot.FindLotByID(db, lotID)
		if err != nil {
			fmt.Printf("can't find lot by ID: %s\n", err)
			errs.ErrorConflict(w, "can't find lot by ID")
			return
		}

		resultJSON, err := json.MarshalIndent(l, "", "    ")
		if err != nil {
			fmt.Printf("can't marshal form: %s\n", err)
			errs.ErrorConflict(w, "can't marshal form")
			return
		}

		w.WriteHeader(http.StatusOK) //status 200
		w.Header().Add("Content-Type", "application/json")
		_, _ = w.Write(resultJSON)*/

		//w.WriteHeader(http.StatusOK) //status 200
		http.Redirect(w, r, "/v1/auction/lots", http.StatusFound)
		fmt.Println("lot created")
	}
}

func GetBuyLot(db *sql.DB, templates map[string]*template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		_, err := session.GetUserIDByCookie(r, db)
		if err != nil {
			errs.ErrorUnauthorized(w)
			return
		}

		lotID, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			fmt.Printf("can't parse lot ID: %s\n", err)
			errs.ErrorConflict(w, "can't parse lot ID")
			return
		}

		l, err := lot.FindLotByID(db, lotID)
		if err != nil {
			fmt.Printf("can't find lot by ID: %s\n", err)
			errs.ErrorConflict(w, "can't find lot by ID")
			return
		}

		w.WriteHeader(http.StatusOK) //status 200
		err = tmplts.RenderTemplate(w, templates, "set_price", l)
		if err != nil {
			fmt.Printf("can't render template: %s\n", err)
			errs.ErrorConflict(w, "can't render template")
			return
		}
	}
}

func PutBuyLot(db *sql.DB, lotCh chan ws.LotWS) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUserID, err := session.GetUserIDByCookie(r, db)
		if err != nil {
			errs.ErrorUnauthorized(w)
			return
		}

		err = r.ParseForm()
		if err != nil {
			fmt.Printf("can't parse form in PutBuyLot: %s\n", err)
			errs.ErrorConflict(w, "can't parse form")
			return
		}

		lotID, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			fmt.Printf("can't parse lot ID: %s\n", err)
			errs.ErrorConflict(w, "can't parse lot ID")
			return
		}

		err = lot.SetPrice(db, r.Form, lotID, currentUserID)
		if err != nil {
			fmt.Printf("can't set lot price: %s\n", err)
			errs.ErrorConflict(w, "can't set lot price")
			return
		}

		l, err := lot.FindLotByID(db, lotID)
		if err != nil {
			fmt.Printf("can't find lot by ID: %s\n", err)
			errs.ErrorConflict(w, "can't find lot by ID")
			return
		}

		lotJSON, err := convertToLotJSON(db, l)
		if err != nil {
			fmt.Printf("can't convert to lot JSON: %s\n", err)
			errs.ErrorConflict(w, "can't convert to lot JSON")
			return
		}

		lotCh <- ws.LotWS{
			Req:          "lot_buy",
			LotIDStr:     strconv.FormatInt(lotID, 10),
			CreatorIDStr: strconv.FormatInt(l.CreatorID, 10),
			BuyerIDStr:   strconv.FormatInt(l.BuyerID, 10),
			Lot:          lotJSON,
		}

		if err = writeResponse(w, db, lotID); err != nil {
			fmt.Printf("can't write response: %s\n", err)
			errs.ErrorConflict(w, "can't write response")
		}

		/*l, err = lot.FindLotByID(db, lotID)
		if err != nil {
			fmt.Printf("can't find lot by ID: %s\n", err)
			errs.ErrorConflict(w, "can't find lot by ID")
			return
		}

		resultJSON, err := json.MarshalIndent(l, "", "    ")
		if err != nil {
			fmt.Printf("can't marshal form: %s\n", err)
			errs.ErrorConflict(w, "can't marshal form")
			return
		}

		w.WriteHeader(http.StatusOK) //status 200
		w.Header().Add("Content-Type", "application/json")
		_, _ = w.Write(resultJSON)*/

		//w.WriteHeader(http.StatusOK) //status 200
		fmt.Printf("price set successfully\n")
	}
}

func GetLotInfo(db *sql.DB, templates map[string]*template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUserID, err := session.GetUserIDByCookie(r, db)
		if err != nil {
			errs.ErrorUnauthorized(w)
			return
		}

		lotID, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			fmt.Printf("can't parse lot ID from URL: %s\n", err)
			errs.ErrorConflict(w, "can't parse lot ID")
			return
		}

		l, err := lot.FindLotByID(db, lotID)
		if err != nil {
			fmt.Printf("can't find lot by ID: %s\n", err)
			errs.ErrorNotFound(w, "can't find lot by ID")
			return
		}

		lotAndUser := struct {
			L      *lot.Lot
			UserID int64
		}{
			L:      &l,
			UserID: currentUserID,
		}

		w.WriteHeader(http.StatusOK) //status 200
		err = tmplts.RenderTemplate(w, templates, "lot_information", lotAndUser)
		if err != nil {
			fmt.Printf("can't render template: %s\n", err)
			errs.ErrorConflict(w, "can't render template")
			return
		}
	}
}

func GetUpdateLot(db *sql.DB, templates map[string]*template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUserID, err := session.GetUserIDByCookie(r, db)
		if err != nil {
			errs.ErrorUnauthorized(w)
			return
		}

		lotID, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			fmt.Printf("can't parse lot ID: %s\n", err)
			errs.ErrorConflict(w, "can't parse lot ID")
			return
		}

		l, err := lot.FindLotByID(db, lotID)
		if err != nil {
			fmt.Printf("can't find lot by ID: %s\n", err)
			errs.ErrorNotFound(w, "can't find lot by ID")
			return
		}

		if currentUserID != l.CreatorID {
			fmt.Printf("can't update lot\n")
			errs.ErrorConflict(w, "can't update lot, not enough rights")
		}

		w.WriteHeader(http.StatusOK) //status 200
		err = tmplts.RenderTemplate(w, templates, "lot_update", l)
		if err != nil {
			fmt.Printf("can't render template: %s\n", err)
			errs.ErrorConflict(w, "can't render template")
			return
		}
	}
}

func PutUpdateLot(db *sql.DB, lotCh chan ws.LotWS) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUserID, err := session.GetUserIDByCookie(r, db)
		if err != nil {
			errs.ErrorUnauthorized(w)
			return
		}

		err = r.ParseForm()
		if err != nil {
			fmt.Printf("can't parse form: %s\n", err)
			errs.ErrorConflict(w, "can't parse form")
			return
		}

		lotID, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			fmt.Printf("can't parse lot ID: %s\n", err)
			errs.ErrorConflict(w, "can't parse lot ID")
			return
		}

		l, err := lot.FindLotByID(db, lotID)
		if err != nil {
			fmt.Printf("can't find lot by ID: %s\n", err)
			errs.ErrorConflict(w, "can't find lot by ID")
			return
		}

		if currentUserID != l.CreatorID {
			fmt.Printf("can't update lot\n")
			fmt.Printf("TODO 111\n")
			errs.ErrorConflict(w, "can't update lot, not enough rights")
			return
		}

		err = lot.UpdateLot(db, r.Form, lotID)
		if err != nil {
			fmt.Printf("can't update lot: %s\n", err)
			errs.ErrorBadRequest(w, "can't update lot")
			return
		}

		l, err = lot.FindLotByID(db, lotID)
		if err != nil {
			fmt.Printf("can't find lot by ID: %s\n", err)
			errs.ErrorConflict(w, "can't find lot by ID")
			return
		}

		lotJSON, err := convertToLotJSON(db, l)
		if err != nil {
			fmt.Printf("can't convert to lot JSON: %s\n", err)
			errs.ErrorConflict(w, "can't convert to lot JSON")
			return
		}

		lotCh <- ws.LotWS{
			Req:          "lot_update",
			LotIDStr:     strconv.FormatInt(l.LotID, 10),
			CreatorIDStr: strconv.FormatInt(l.CreatorID, 10),
			BuyerIDStr:   strconv.FormatInt(l.BuyerID, 10),
			Lot:          lotJSON,
		}

		if err = writeResponse(w, db, lotID); err != nil {
			fmt.Printf("can't write response: %s\n", err)
			errs.ErrorConflict(w, "can't write response")
		}

		/*l, err = lot.FindLotByID(db, lotID)
		if err != nil {
			fmt.Printf("can't find lot by ID: %s\n", err)
			errs.ErrorConflict(w, "can't find lot by ID")
			return
		}

		resultJSON, err := json.MarshalIndent(l, "", "    ")
		if err != nil {
			fmt.Printf("can't marshal form: %s\n", err)
			errs.ErrorConflict(w, "can't marshal form")
			return
		}

		w.WriteHeader(http.StatusOK) //status 200
		w.Header().Add("Content-Type", "application/json")
		_, _ = w.Write(resultJSON)*/

		//w.WriteHeader(http.StatusOK) //status 200
		fmt.Printf("lot update successfully\n")
	}
}

func DeleteLot(db *sql.DB, lotCh chan ws.LotWS) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUserID, err := session.GetUserIDByCookie(r, db)
		if err != nil {
			errs.ErrorUnauthorized(w)
			return
		}

		lotID, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			fmt.Printf("can't parse lot ID: %s\n", err)
			errs.ErrorConflict(w, "can't parse lot ID")
			return
		}

		l, err := lot.FindLotByID(db, lotID)
		if err != nil {
			fmt.Printf("can't find lot by ID: %s\n", err)
			errs.ErrorNotFound(w, "can't find lot by ID")
			return
		}

		if l.CreatorID != currentUserID {
			fmt.Println("can't delete lot, not enough rights")
			errs.ErrorConflict(w, "can't delete lot not enough rights")
			return
		}

		checkStatus(w, l)

		err = lot.SoftDeleteLot(db, lotID)
		if err != nil {
			fmt.Printf("can't delete lot: %s\n", err)
			errs.ErrorConflict(w, "can't delete lot")
			return
		}

		l, err = lot.FindLotByID(db, lotID)
		if err != nil {
			fmt.Printf("can't find lot by ID: %s\n", err)
			errs.ErrorNotFound(w, "can't find lot by ID")
			return
		}

		lotJSON, err := convertToLotJSON(db, l)
		if err != nil {
			fmt.Printf("can't convert to lot JSON: %s\n", err)
			errs.ErrorConflict(w, "can't convert to lot JSON")
			return
		}

		lotCh <- ws.LotWS{
			Req:      "lot_delete",
			LotIDStr: strconv.FormatInt(l.LotID, 10),
			Lot:      lotJSON,
		}

		if err = writeResponse(w, db, lotID); err != nil {
			fmt.Printf("can't write response: %s\n", err)
			errs.ErrorConflict(w, "can't write response")
		}

		fmt.Println("lot deleted")
	}
}

func RoutingAuction(r *chi.Mux, db *sql.DB, templates map[string]*template.Template,
	lotCh chan ws.LotWS) {

	go agingCheck(db, lotCh)

	r.Route("/v1/auction/lots", func(r chi.Router) {
		r.Get("/", GetLots(db, templates))
		r.Get("/json", GetLotsJSON(db))
		r.Get("/create", GetCreateLot(db, templates))
		r.Post("/", PostCreateLot(db, lotCh))
		r.Get("/{id}/buy", GetBuyLot(db, templates))
		r.Put("/{id}/buy", PutBuyLot(db, lotCh))
		r.Get("/{id}", GetLotInfo(db, templates))
		r.Get("/{id}/update", GetUpdateLot(db, templates))
		r.Put("/{id}", PutUpdateLot(db, lotCh))
		r.Delete("/{id}", DeleteLot(db, lotCh))
	})
}
