package main

import (
	"database/sql"
	"html/template"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/gorilla/websocket"
	_ "github.com/lib/pq"
	"gitlab.com/maxboychuk/course-project/cmd/auction-api"
	"gitlab.com/maxboychuk/course-project/cmd/gateway-api"
	"gitlab.com/maxboychuk/course-project/pkg/ws"
)

func main() {
	dsn := "postgres://max:pswd@localhost:5432"
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatalf("can't connect to db: %s", err)
	}
	if err = db.Ping(); err != nil {
		log.Fatalf("can't ping db: %s", err)
	}

	var (
		wsClients = ws.ClientsWS{
			WSConn: make(map[string]*websocket.Conn),
		}

		upgrader = websocket.Upgrader{
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
		}

		lotCh     = make(chan ws.LotWS)
		templates = make(map[string]*template.Template)
	)

	r := chi.NewRouter()
	go gateway.RoutingGateway(r, db, templates)
	go auction.RoutingAuction(r, db, templates, lotCh)
	go ws.RoutingWS(r, upgrader, &wsClients, lotCh)
	if err = http.ListenAndServe(":9000", r); err != nil {
		log.Fatalf("can't route: %s", err)
	}
}
