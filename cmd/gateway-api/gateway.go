package gateway

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"github.com/rs/xid"
	"gitlab.com/maxboychuk/course-project/internal/auth"
	"gitlab.com/maxboychuk/course-project/internal/session"
	"gitlab.com/maxboychuk/course-project/internal/user"
	"gitlab.com/maxboychuk/course-project/pkg/errs"
	"gitlab.com/maxboychuk/course-project/pkg/lot"
	"gitlab.com/maxboychuk/course-project/pkg/tmplts"
)

func GetMainPage(templates map[string]*template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK) //status 200
		err := tmplts.RenderTemplate(w, templates, "main_page", nil)
		if err != nil {
			fmt.Printf("can't render template: %s\n", err)
			errs.ErrorConflict(w, "can't render template")
			return
		}
	}
}

func GetSignUp(templates map[string]*template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK) //status 200
		err := tmplts.RenderTemplate(w, templates, "sign_up", nil)
		if err != nil {
			fmt.Printf("can't render template: %s\n", err)
			errs.ErrorConflict(w, "can't render template")
			return
		}
	}
}

func PostSignUp(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseForm()
		if err != nil {
			fmt.Printf("can't parse form: %s\n", err)
			errs.ErrorConflict(w, "can't parse form")
			return
		}

		email := r.FormValue("email")
		if _, err = user.FindUserByEmail(db, email); err == nil {
			fmt.Printf("email %s already exists!\n", email)
			errs.ErrorConflict(w, "email already exists")
			return
		}

		err = user.CreateUser(db, r.Form, time.Now())
		if err != nil {
			fmt.Printf("can't create user %s: %s\n", email, err)
			errs.ErrorConflict(w, "can't create user")
			return
		}

		//w.WriteHeader(http.StatusCreated) //status 201
		http.Redirect(w, r, "/v1/auction/main_page", http.StatusFound)
		fmt.Printf("user '%s' sign up\n", email)
	}
}

func GetSignIn(templates map[string]*template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK) //status 200
		err := tmplts.RenderTemplate(w, templates, "sign_in", nil)
		if err != nil {
			fmt.Printf("can't render template: %s\n", err)
			errs.ErrorConflict(w, "can't render template")
			return
		}
	}
}

func PostSignIn(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseForm()
		if err != nil {
			fmt.Printf("can't parse form in sign in: %s\n", err)
			errs.ErrorConflict(w, "can't parse form")
			return
		}

		email := r.FormValue("email")
		err = auth.AuthorizationUser(db, email, r.FormValue("password"))
		if err != nil {
			fmt.Printf("user '%s' can't sign in: %s\n", email, err)
			errs.ErrorUnauthorized(w)
			return
		}

		u, err := user.FindUserByEmail(db, email)
		if err != nil {
			fmt.Printf("can't find user %s in sign in\n", email)
			errs.ErrorConflict(w, "can't find user")
			return
		}

		t := time.Now()
		bearer := xid.New().String()
		cookie := http.Cookie{
			Name:     "cookie",
			Value:    bearer,
			Domain:   "localhost",
			Path:     "/",
			Secure:   false,
			Expires:  t.Add(time.Hour * 24),
			HttpOnly: true,
		}
		http.SetCookie(w, &cookie)
		err = session.AddSession(db, bearer, u.ID, t)
		if err != nil {
			fmt.Printf("can't add session: %s\n", err)
			errs.ErrorConflict(w, "can't add session")
			return
		}

		//w.WriteHeader(http.StatusOK) //status 200
		http.Redirect(w, r, "/v1/auction/users/0", http.StatusFound)
		fmt.Printf("user '%s' sign in\n", email)
	}
}

func GetUserInfo(db *sql.DB, templates map[string]*template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUserID, err := session.GetUserIDByCookie(r, db)
		if err != nil {
			fmt.Printf("can't get user ID by cookie\n")
			errs.ErrorUnauthorized(w)
			return
		}

		userID, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			fmt.Printf("can't parse user ID: %s\n", err)
			errs.ErrorConflict(w, "can't parse user ID")
			return
		}

		var u user.User
		isCurrentUser := false
		if (userID == 0) || (userID == currentUserID) {
			isCurrentUser = true
			u, err = user.FindUserByID(db, currentUserID)
		} else {
			u, err = user.FindUserByID(db, userID)
		}
		if err != nil {
			fmt.Printf("can't find user by ID: %s\n", err)
			errs.ErrorNotFound(w, "can't find user by ID")
			return
		}

		userModel := struct {
			User          user.User
			IsCurrentUser bool
		}{
			User:          u,
			IsCurrentUser: isCurrentUser,
		}

		w.WriteHeader(http.StatusOK) //status 200
		err = tmplts.RenderTemplate(w, templates, "user_information", userModel)
		if err != nil {
			fmt.Printf("can't render template: %s\n", err)
			errs.ErrorConflict(w, "can't render template")
			return
		}
	}
}

func GetUpdateUser(db *sql.DB, templates map[string]*template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUserID, err := session.GetUserIDByCookie(r, db)
		if err != nil {
			fmt.Printf("can't get user ID by cookie\n")
			errs.ErrorUnauthorized(w)
			return
		}

		userID, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			fmt.Printf("can't parse user ID: %s\n", err)
			errs.ErrorConflict(w, "can't parse user ID")
			return
		}

		if (userID != 0) && (userID != currentUserID) {
			fmt.Println("can't get user information, not enough rights")
			errs.ErrorConflict(w, "can't get user information, not enough rights")
			return
		}

		u, err := user.FindUserByID(db, currentUserID)
		if err != nil {
			fmt.Printf("can't find user by ID: %s\n", err)
			errs.ErrorNotFound(w, "can't find user by ID")
			return
		}

		w.WriteHeader(http.StatusOK) //status 200
		err = tmplts.RenderTemplate(w, templates, "user_update", u)
		if err != nil {
			fmt.Printf("can't render template: %s\n", err)
			errs.ErrorConflict(w, "can't render template")
			return
		}
	}
}

func PutUpdateUser(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUserID, err := session.GetUserIDByCookie(r, db)
		if err != nil {
			fmt.Printf("can't get user ID by cookie\n")
			errs.ErrorUnauthorized(w)
			return
		}

		userID, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			fmt.Printf("can't parse user ID: %s\n", err)
			errs.ErrorConflict(w, "can't parse user ID")
			return
		}

		if (userID != 0) && (userID != currentUserID) {
			fmt.Println("can't get user information, not enough rights")
			errs.ErrorConflict(w, "can't get user information, not enough rights")
			return
		}

		err = r.ParseForm()
		if err != nil {
			fmt.Printf("can't parse form: %s\n", err)
			errs.ErrorConflict(w, "can't parse form")
			return
		}

		err = user.UpdateUser(db, currentUserID, r.Form)
		if err != nil {
			fmt.Printf("can't update user: %s\n", err)
			errs.ErrorConflict(w, "can't update user")
			return
		}

		u, err := user.FindUserByID(db, currentUserID)
		if err != nil {
			fmt.Printf("can't find user by ID: %s\n", err)
			errs.ErrorNotFound(w, "can't find user by ID")
			return
		}

		resultJSON, err := json.MarshalIndent(u, "", "    ")
		if err != nil {
			fmt.Printf("can't marshal form: %s\n", err)
			errs.ErrorConflict(w, "can't marshal form")
			return
		}

		w.WriteHeader(http.StatusOK) //status 200
		w.Header().Add("Content-Type", "application/json")
		_, _ = w.Write(resultJSON)

		fmt.Printf("user '%+v' update\n", currentUserID)
	}
}

func GetUserLotsHTML(db *sql.DB, templates map[string]*template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUserID, err := session.GetUserIDByCookie(r, db)
		if err != nil {
			errs.ErrorUnauthorized(w)
			return
		}

		/*userID, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			fmt.Printf("can't parse user ID: %s\n", err)
			errorConflict(w, "can't parse user ID")
			return
		}

		if userID == 0 {
			userID = currentUserID
		}*/

		lots, err := lot.GetLots(db)
		if err != nil {
			fmt.Printf("can't get user lots: %s", err)
			errs.ErrorNotFound(w, "can't get user lots")
			return
		}

		typeLots := r.URL.Query().Get("type")
		if (typeLots != "own") && (typeLots != "buyed") {
			typeLots = ""
		}

		filterLots := struct {
			Lots     []lot.Lot
			UserID   int64
			TypeLots string
		}{
			Lots:     lots,
			UserID:   currentUserID,
			TypeLots: typeLots,
		}

		err = tmplts.RenderTemplate(w, templates, "lots_html_format", filterLots)
		if err != nil {
			fmt.Printf("can't render template: %s\n", err)
			errs.ErrorConflict(w, "can't render template")
			return
		}
	}
}

func GetUserLotsJSON(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUserID, err := session.GetUserIDByCookie(r, db)
		if err != nil {
			errs.ErrorUnauthorized(w)
			return
		}

		userID, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			fmt.Printf("can't parse user ID: %s\n", err)
			errs.ErrorConflict(w, "can't parse user ID")
			return
		}

		if userID == 0 {
			userID = currentUserID
		}

		typeLots := r.URL.Query().Get("type")
		userLots, err := lot.GetUserLots(db, userID, typeLots)
		if err != nil {
			fmt.Printf("can't get user lots: %s\n", err)
			errs.ErrorConflict(w, "can't get user lots")
			return
		}

		resultJSON, err := json.MarshalIndent(userLots, "", "    ")
		if err != nil {
			fmt.Printf("can't marshal to json: %s\n", err)
			errs.ErrorConflict(w, "can't marshal to json")
			return
		}

		w.WriteHeader(http.StatusOK) //OK 200
		w.Header().Add("Content-Type", "application/json")
		_, _ = w.Write(resultJSON)
	}
}

func RoutingGateway(r *chi.Mux, db *sql.DB, templates map[string]*template.Template) {
	r.Route("/v1/auction", func(r chi.Router) {
		r.Get("/main_page", GetMainPage(templates))
		r.Get("/sign_up", GetSignUp(templates))
		r.Post("/sign_up", PostSignUp(db))
		r.Get("/sign_in", GetSignIn(templates))
		r.Post("/sign_in", PostSignIn(db))

		r.Get("/users/{id}", GetUserInfo(db, templates))
		r.Get("/users/{id}/update", GetUpdateUser(db, templates))
		r.Put("/users/{id}", PutUpdateUser(db))
		r.Get("/users/{id}/lots", GetUserLotsHTML(db, templates))
		r.Get("/users/{id}/lots/json", GetUserLotsJSON(db))
	})
}
