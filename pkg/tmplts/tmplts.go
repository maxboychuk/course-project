package tmplts

import (
	"html/template"
	"io"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
)

func RenderTemplate(w io.Writer, templates map[string]*template.Template,
	name string, viewModel interface{}) error {
	tmpl, ok := templates[name]
	if !ok {
		funcMap := template.FuncMap{
			"addStep": func(price float64, step float64) float64 {
				return price + step
			},

			"toString": func(i int64) string {
				return strconv.FormatInt(i, 10)
			},

			"toBirthday": func(t time.Time) string {
				return strings.Split(t.String(), " ")[0]
			},
		}

		tmplBase := "../../templates/base.html"
		tmpl = template.Must(template.New(name).Funcs(funcMap).
			ParseFiles("../../templates/"+name+".html", tmplBase))
		templates[name] = tmpl
	}

	err := tmpl.ExecuteTemplate(w, "base", viewModel)
	if err != nil {
		return errors.Wrap(err, "can't execute template in RenderTemplate")
	}

	return nil
}
