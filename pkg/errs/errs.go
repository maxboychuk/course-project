package errs

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type errorData struct {
	error string
}

func ErrorUnauthorized(w http.ResponseWriter) {
	data := errorData{"unauthorized request"}
	jsonData, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		fmt.Printf("can't marshal json: %s\n", err)
	}
	w.WriteHeader(http.StatusUnauthorized) //status 401
	w.Header().Add("Content-Type", "application/json")
	_, _ = w.Write(jsonData)
}

func ErrorNotFound(w http.ResponseWriter, descr string) {
	data := errorData{"content not found: " + descr}
	jsonData, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		fmt.Printf("can't marshal json: %s\n", err)
	}
	w.WriteHeader(http.StatusNotFound) //status 404
	w.Header().Add("Content-Type", "application/json")
	_, _ = w.Write(jsonData)
}

func ErrorConflict(w http.ResponseWriter, descr string) {
	data := errorData{"conflict: " + descr}
	jsonData, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		fmt.Printf("can't marshal json: %s\n", err)
	}
	w.WriteHeader(http.StatusConflict) //status 409
	w.Header().Add("Content-Type", "application/json")
	_, _ = w.Write(jsonData)
}

func ErrorBadRequest(w http.ResponseWriter, descr string) {
	data := errorData{"invalid input: " + descr}
	jsonData, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		fmt.Printf("can't marshal json: %s\n", err)
	}
	w.WriteHeader(http.StatusBadRequest) //status 400
	w.Header().Add("Content-Type", "application/json")
	_, _ = w.Write(jsonData)
}
