package ws

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sync"

	"github.com/go-chi/chi"
	"github.com/gorilla/websocket"
	"github.com/rs/xid"
	"gitlab.com/maxboychuk/course-project/pkg/lot"
)

type (
	ClientsWS struct {
		WSConn map[string]*websocket.Conn
		sync.Mutex
	}

	LotWS struct {
		Req          string
		LotIDStr     string
		CreatorIDStr string
		BuyerIDStr   string
		Lot          lot.JSONLot
	}
)

func (clients *ClientsWS) AddClient(conn *websocket.Conn) {
	clients.Mutex.Lock()
	clients.WSConn[xid.New().String()] = conn
	clients.Mutex.Unlock()
	fmt.Printf("added client, total clients: %d\n", len(clients.WSConn))
}

func (clients *ClientsWS) removeClientByID(key string) {
	clients.Mutex.Lock()
	if _, ok := clients.WSConn[key]; ok {
		delete(clients.WSConn, key)
	}
	clients.Mutex.Unlock()
	fmt.Printf("removed client #%s, total clients %d\n", key, len(clients.WSConn))
}

func (clients *ClientsWS) BroadcastMessage(message []byte) {
	for key, c := range clients.WSConn {
		err := c.WriteMessage(websocket.TextMessage, message)
		if err != nil {
			fmt.Printf("can't broadcast message: %+v\n", err)
			clients.removeClientByID(key)
		}
	}
}

func LotsUpdateWS(upgrader websocket.Upgrader, wsClients *ClientsWS, lotCh chan LotWS) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			fmt.Printf("can't upgrade connection: %s\n", err)
			return
		}
		defer conn.Close()

		wsClients.AddClient(conn)

		for {
			updLot := <-lotCh
			res, err := json.Marshal(updLot)
			if err != nil {
				fmt.Printf("can't marshal message: %+v\n", err)
				continue
			}
			wsClients.BroadcastMessage(res)
		}
	}
}

func RoutingWS(r *chi.Mux, upgrader websocket.Upgrader, wsClients *ClientsWS, lotCh chan LotWS) {
	r.Route("/v1/auction/ws", func(r chi.Router) {
		r.HandleFunc("/", LotsUpdateWS(upgrader, wsClients, lotCh))
	})
}
