package lot

import (
	"database/sql"
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/maxboychuk/course-project/internal/user"
)

const lotFields = `id, creator_id, buyer_id, title, description, ` +
	`current_price, price_step, status, created_at, updated_at, end_at, deleted_at`

type (
	BaseLot struct {
		LotID        int64     `json:"lot_id"`
		Title        string    `json:"title"`
		Description  string    `json:"description"`
		CurrentPrice float64   `json:"current_price"`
		PriceStep    float64   `json:"price_step"`
		Status       string    `json:"status"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
		EndAt        time.Time `json:"end_at"`
		DeletedAt    time.Time `json:"deleted_at"`
	}

	Lot struct {
		BaseLot
		CreatorID int64 `json:"creator_id"`
		BuyerID   int64 `json:"buyer_id"`
	}

	JSONLot struct {
		BaseLot
		Creator user.ShortUser `json:"creator"`
		Buyer   user.ShortUser `json:"buyer"`
	}

	CreateUpdateLot struct {
		Title        string    `json:"title"`
		Description  string    `json:"description"`
		CurrentPrice float64   `json:"current_price"`
		PriceStep    float64   `json:"price_step"`
		Status       string    `json:"status"`
		EndAt        time.Time `json:"end_at"`
	}

	BuyLot struct {
		CurrentPrice float64 `json:"current_price"`
	}

	SQLScanner interface {
		Scan(dest ...interface{}) error
	}
)

func (l *Lot) IsCreator(userID int64) bool {
	return l.CreatorID == userID
}

func (l *Lot) IsBuyer(userID int64) bool {
	return l.BuyerID == userID
}

func CreateLot(db *sql.DB, form map[string][]string, userID int64, t time.Time) (int64, error) {
	price, err := strconv.ParseFloat(form["price"][0], 64)
	if err != nil {
		return 0, errors.Wrap(err, "can't parse price to int64")
	}
	step, err := strconv.ParseFloat(form["price_step"][0], 64)
	if err != nil {
		return 0, errors.Wrap(err, "can't parse step to int64")
	}

	var lotID int64
	for {
		lotID = rand.Int63()
		_, err = FindLotByID(db, lotID)
		if err != nil {
			break
		}
	}

	endTime := t.Add(time.Second * time.Duration(86400))
	l := CreateUpdateLot{
		Title:        form["title"][0],
		Description:  form["description"][0],
		CurrentPrice: price,
		PriceStep:    step,
		Status:       "created",
		EndAt:        endTime,
	}

	const insertLotQuery = `INSERT INTO lots(` + lotFields + `)` +
		`VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)`
	_, err = db.Exec(insertLotQuery, lotID, userID, 0, l.Title,
		l.Description, l.CurrentPrice, l.PriceStep, l.Status, t, t, l.EndAt, time.Time{})
	if err != nil {
		return 0, errors.Wrapf(err, "can't insert user %s", err)
	}

	/*err = user.AddUserLot(db, userID, lotID)
	if err != nil {
		return 0, errors.Wrap(err, "can't add lot in user")
	}*/

	return lotID, nil
}

func UpdateLot(db *sql.DB, form map[string][]string, lotID int64) error {
	lotStatus := form["status"][0]
	if lotStatus == "finished" {
		err := SoftDeleteLot(db, lotID)
		if err != nil {
			return errors.Wrap(err, "can't delete lot in database")
		}
	} else {
		newPrice, err := strconv.ParseFloat(form["price"][0], 64)
		if err != nil {
			return errors.Wrap(err, "can't parse price to float")
		}

		newStep, err := strconv.ParseFloat(form["price_step"][0], 64)
		if err != nil {
			return errors.Wrap(err, "can't parse price_step to float")
		}

		flag := false
		if lotStatus == "created" {
			flag = true
		}

		fmt.Println(form["description"][0])

		l := CreateUpdateLot{
			Title:        form["title"][0],
			Description:  form["description"][0],
			CurrentPrice: newPrice,
			PriceStep:    newStep,
			Status:       lotStatus,
		}
		fmt.Println("HERE1")

		if flag {
			const updateLotQuery = `UPDATE lots SET title = $1, description = $2, ` +
				`current_price = $3, price_step = $4, buyer_id = $5, status = $6, updated_at = $7 WHERE id = $8`
			_, err = db.Exec(updateLotQuery, l.Title, l.Description,
				l.CurrentPrice, l.PriceStep, 0, l.Status, time.Now(), lotID)
		} else {
			const updateLotQuery = `UPDATE lots SET title = $1, description = $2, ` +
				`current_price = $3, price_step = $4, status = $5, updated_at = $6 WHERE id = $7`
			_, err = db.Exec(updateLotQuery, l.Title, l.Description,
				l.CurrentPrice, l.PriceStep, l.Status, time.Now(), lotID)
		}
		if err != nil {
			return errors.Wrap(err, "can't update lot in database")
		}
	}

	return nil
}

func SetPrice(db *sql.DB, form map[string][]string, lotID int64, buyerID int64) error {
	l, err := FindLotByID(db, lotID)
	if err != nil {
		return errors.Wrapf(err, "can't find lot by ID %+v", lotID)
	}

	newPrice, err := strconv.ParseFloat(form["price"][0], 64)
	if err != nil {
		return errors.Wrap(err, "can't parse new_price to float")
	}

	if newPrice <= l.CurrentPrice {
		return errors.Wrap(err, "can't set price, price less")
	}

	/*if newPrice % l.PriceStep != 0 {

	}*/

	buyL := BuyLot{
		CurrentPrice: newPrice,
	}

	const updateLotQuery = `UPDATE lots SET current_price = $1, ` +
		`buyer_id = $2, updated_at = $3 WHERE id = $4`
	_, err = db.Exec(updateLotQuery, buyL.CurrentPrice, buyerID, time.Now(), lotID)
	if err != nil {
		return errors.Wrapf(err, "can't update lot in database: %s", err)
	}

	return nil
}

func SoftDeleteLot(db *sql.DB, lotID int64) error {
	l := CreateUpdateLot{
		Status: "finished",
	}
	const updateLotQuery = `UPDATE lots SET status = $1, updated_at = $2, ` +
		`deleted_at = $3 WHERE id = $4`
	_, err := db.Exec(updateLotQuery, l.Status, time.Now(), time.Now(), lotID)
	if err != nil {
		return errors.Wrapf(err, "can't update lot in database: %s", err)
	}

	return nil
}

/*func GetLotsByID(db *sql.DB, lotIDs []int64) ([]Lot, error) {
	var lots []Lot
	for _, id := range lotIDs {
		l, err := FindLotByID(db, id)
		if err != nil {
			return nil, errors.Wrap(err, "can't find lot by id")
		}
		lots = append(lots, l)
	}

	return lots, nil
}*/

func GetLots(db *sql.DB) ([]Lot, error) {
	lots, err := scanLots(db)
	if err != nil {
		return nil, errors.Wrap(err, "can't scan lots")
	}

	return lots, nil
}

func scanLot(scanner SQLScanner, l *Lot) error {
	return scanner.Scan(&l.LotID, &l.CreatorID, &l.BuyerID, &l.Title,
		&l.Description, &l.CurrentPrice, &l.PriceStep, &l.Status,
		&l.CreatedAt, &l.UpdatedAt, &l.EndAt, &l.DeletedAt,
	)
}

func scanLots2(rows *sql.Rows) ([]Lot, error) {
	var lots []Lot
	var err error
	for rows.Next() {
		var l Lot
		if err = scanLot(rows, &l); err != nil {
			return nil, errors.Wrap(err, "can't scan user")
		}
		lots = append(lots, l)
	}

	if err = rows.Err(); err != nil {
		return nil, errors.Wrap(err, "rows return error")
	}

	return lots, nil
}

func scanLots(db *sql.DB) ([]Lot, error) {
	const selectUsersQuery = `SELECT ` + lotFields + ` FROM lots ORDER BY created_at DESC`
	rows, err := db.Query(selectUsersQuery)
	if err != nil {
		return nil, errors.Wrap(err, "can't exec query")
	}
	defer rows.Close()

	users, err := scanLots2(rows)
	if err != nil {
		return nil, errors.Wrap(err, "can't scan lots")
	}

	return users, nil
}

func FindLotByID(db *sql.DB, id int64) (Lot, error) {
	const findLotByIDQuery = `SELECT ` + lotFields + ` FROM lots WHERE id = $1`
	rows, err := db.Query(findLotByIDQuery, id)
	if err != nil {
		return Lot{}, errors.Wrap(err, "can't exec query")
	}
	defer rows.Close()

	lots, err := scanLots2(rows)
	if err != nil {
		return Lot{}, errors.Wrap(err, "can't scan lots")
	}

	if len(lots) == 0 {
		return Lot{}, errors.New("can't find lot " + string(id))
	}

	return lots[0], nil
}

func getUserLot(db *sql.DB, l Lot) (JSONLot, error) {
	creator, err := user.FindShortUserByID(db, l.CreatorID)
	if err != nil {
		return JSONLot{}, errors.Wrap(err, "can't find short user by ID")
	}

	buyer := user.ShortUser{}
	if l.BuyerID != 0 {
		buyer, err = user.FindShortUserByID(db, l.BuyerID)
		if err != nil {
			return JSONLot{}, errors.Wrap(err, "can't find short user by ID")
		}
	}

	newLot := JSONLot{
		BaseLot: BaseLot{
			LotID:        l.LotID,
			Title:        l.Title,
			Description:  l.Description,
			CurrentPrice: l.CurrentPrice,
			PriceStep:    l.PriceStep,
			Status:       l.Status,
			CreatedAt:    l.CreatedAt,
			UpdatedAt:    l.UpdatedAt,
			EndAt:        l.EndAt,
			DeletedAt:    l.DeletedAt,
		},
		Creator: creator,
		Buyer:   buyer,
	}

	return newLot, nil
}

func getOwnLots(db *sql.DB, lots []Lot, userID int64) ([]JSONLot, error) {
	userLots := make([]JSONLot, 0)
	for _, l := range lots {
		if l.CreatorID == userID {
			newLot, err := getUserLot(db, l)
			if err != nil {
				return nil, errors.Wrap(err, "can't get user lot")
			}
			userLots = append(userLots, newLot)
		}
	}
	return userLots, nil
}

func getBuyedLots(db *sql.DB, lots []Lot, userID int64) ([]JSONLot, error) {
	userLots := make([]JSONLot, 0)
	for _, l := range lots {
		if l.BuyerID == userID {
			newLot, err := getUserLot(db, l)
			if err != nil {
				return nil, errors.Wrap(err, "can't get user lot")
			}
			userLots = append(userLots, newLot)
		}
	}
	return userLots, nil
}

func getAllUserLots(db *sql.DB, lots []Lot, userID int64) ([]JSONLot, error) {
	userLots := make([]JSONLot, 0)
	for _, l := range lots {
		if (l.CreatorID == userID) || (l.BuyerID == userID) {
			newLot, err := getUserLot(db, l)
			if err != nil {
				return nil, errors.Wrap(err, "can't get user lot")
			}
			userLots = append(userLots, newLot)
		}
	}
	return userLots, nil
}

func GetUserLots(db *sql.DB, userID int64, typeLots string) ([]JSONLot, error) {
	lots, err := GetLots(db)
	if err != nil {
		return nil, errors.Wrap(err, "can't get lots")
	}

	var userLots []JSONLot
	switch typeLots {
	case "own":
		userLots, err = getOwnLots(db, lots, userID)
		if err != nil {
			return nil, errors.Wrap(err, "can't get own user lots")
		}
	case "buyed":
		userLots, err = getBuyedLots(db, lots, userID)
		if err != nil {
			return nil, errors.Wrap(err, "can't get buyed user lots")
		}
	default:
		userLots, err = getAllUserLots(db, lots, userID)
		if err != nil {
			return nil, errors.Wrap(err, "can't get all user lots")
		}
	}

	return userLots, nil
}
