package lot

import (
	"testing"
)

func TestLot_IsCreator(t *testing.T) {
	userID := int64(5577006791947779410)
	l := Lot{
		CreatorID: int64(5577006791947779410),
	}
	if !l.IsCreator(userID) {
		t.Errorf("l.IsCreator(%+v) = %+v, want = %+v", userID, false, true)
	}
}
