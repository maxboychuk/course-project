package session

import (
	"database/sql"
	"net/http"
	"time"

	"github.com/pkg/errors"
)

const sessionFields = `session_id, user_id, created_at`

type SQLScanner interface {
	Scan(dest ...interface{}) error
}

type Session struct {
	SessionID  string    `json:"session_id"`
	UserID     int64     `json:"user_id"`
	CreatedAt  time.Time `json:"created_at"`
	ValidUntil time.Time `json:"valid_until"`
}

func AddSession(db *sql.DB, sessionID string,
	userID int64, t time.Time) error {
	s := &Session{
		SessionID:  sessionID,
		UserID:     userID,
		CreatedAt:  t,
		ValidUntil: t.Add(time.Hour * 24),
	}

	const insertSessionQuery = `INSERT INTO sessions(` + sessionFields + `)` +
		`VALUES ($1, $2, $3)`
	_, err := db.Exec(insertSessionQuery, s.SessionID, s.UserID, s.CreatedAt)
	if err != nil {
		return errors.Wrapf(err, "can't insert user %s", err)
	}

	return nil
}

func scanSession(scanner SQLScanner, s *Session) error {
	return scanner.Scan(&s.SessionID, &s.UserID, &s.CreatedAt)
}

func FindSessionByID(db *sql.DB, id string) (Session, error) {
	const findUserByEmailQuery = `SELECT ` + sessionFields + ` FROM sessions WHERE session_id = $1`
	row := db.QueryRow(findUserByEmailQuery, id)

	var s Session
	err := scanSession(row, &s)
	if err != nil {
		return Session{}, errors.Wrap(err, "can't scan user")
	}

	return s, nil
}

func GetUserIDByCookie(r *http.Request, db *sql.DB) (int64, error) {
	cookie, err := r.Cookie("cookie")
	if err != nil {
		return 0, errors.Wrap(err, "can't find cookie")
	}

	s, err := FindSessionByID(db, cookie.Value)
	if err != nil {
		return 0, errors.Wrap(err, "can't get session with cookie")
	}

	return s.UserID, nil
}
