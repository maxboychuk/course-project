package user

import (
	"database/sql"
	"fmt"
	"math/rand"
	"time"

	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

const userFields = `id, first_name, last_name, birthday, email, password, ` +
	`created_at, updated_at`

type User struct {
	ID        int64     `json:"id"`
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
	Birthday  time.Time `json:"birthday"`
	Email     string    `json:"email"`
	Password  string    `json:"password"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type ShortUser struct {
	ID        int64  `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

type SQLScanner interface {
	Scan(dest ...interface{}) error
}

func createHashPassword(pwd []byte) (string, error) {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.DefaultCost)
	if err != nil {
		return "", errors.Wrap(err, "can't generate password hash")
	}

	return string(hash), nil
}

func CreateUser(db *sql.DB, form map[string][]string, t time.Time) error {
	date := form["birthday"][0]
	if date == "" {
		date = "0001-01-01"
	}
	birthday, err := time.Parse("2006-01-02", date)
	if err != nil {
		fmt.Printf("can't parse birthday by user %s: %s", form["email"][0], err)
	}

	hash, err := createHashPassword([]byte(form["password"][0]))
	if err != nil {
		return errors.Wrapf(err, "can't add user: %s", err)
	}

	var userID int64
	for {
		userID = rand.Int63()
		_, err = FindUserByID(db, userID)
		if err != nil {
			break
		}
	}

	u := User{
		ID:        userID,
		FirstName: form["first_name"][0],
		LastName:  form["last_name"][0],
		Birthday:  birthday,
		Email:     form["email"][0],
		Password:  hash,
	}

	const insertUserQuery = `INSERT INTO users(` + userFields + `)` +
		`VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`
	_, err = db.Exec(insertUserQuery, u.ID, u.FirstName, u.LastName,
		u.Birthday, u.Email, u.Password, t, t)
	if err != nil {
		return errors.Wrapf(err, "can't insert user %s", err)
	}

	return nil
}

func UpdateUser(db *sql.DB, userID int64, form map[string][]string) error {
	_, err := FindUserByID(db, userID)
	if err != nil {
		return errors.Wrapf(err, "can't find user by id %+v: %s", userID, err)
	}

	brthd := ""
	if form["birthday"] == nil {
		brthd = "0001-01-01"
	} else {
		brthd = form["birthday"][0]
	}
	birthday, err := time.Parse("2006-01-02", brthd)
	if err != nil {
		fmt.Printf("can't parse birthday by user %+v: %s", userID, err)
	}

	if (form["first_name"] == nil) || (form["last_name"] == nil) {
		return errors.New("first or last name not defined")
	}

	u := User{
		FirstName: form["first_name"][0],
		LastName:  form["last_name"][0],
		Birthday:  birthday,
	}

	const updateUserQuery = `UPDATE users SET first_name = $1,` +
		`last_name = $2, birthday = $3, updated_at = $4 WHERE id = $5`
	_, err = db.Exec(updateUserQuery, u.FirstName, u.LastName,
		u.Birthday, time.Now(), userID)
	if err != nil {
		return errors.Wrapf(err, "can't update user '%+v' in database: %s", userID, err)
	}

	return nil
}

func scanUser(scanner SQLScanner, u *User) error {
	return scanner.Scan(&u.ID, &u.FirstName, &u.LastName, &u.Birthday, &u.Email,
		&u.Password, &u.CreatedAt, &u.UpdatedAt,
	)
}

/*func scanUsers(rows *sql.Rows) ([]User, error) {
	var users []User
	var err error
	for rows.Next() {
		var u User
		if err = scanUser(rows, &u); err != nil {
			return nil, errors.Wrap(err, "can't scan user")
		}

		users = append(users, u)
	}

	if err = rows.Err(); err != nil {
		return nil, errors.Wrap(err, "rows return error")
	}

	return users, nil
}*/

func FindUserByEmail(db *sql.DB, email string) (User, error) {
	const findUserByEmailQuery = `SELECT ` + userFields + ` FROM users WHERE email = $1`
	row := db.QueryRow(findUserByEmailQuery, email)

	var u User
	err := scanUser(row, &u)
	if err != nil {
		return User{}, errors.Wrap(err, "can't scan user")
	}

	return u, nil
}

func FindShortUserByID(db *sql.DB, id int64) (ShortUser, error) {
	u, err := FindUserByID(db, id)
	if err != nil {
		return ShortUser{}, errors.Wrap(err, "can't find user")
	}

	shortU := ShortUser{
		ID:        u.ID,
		FirstName: u.FirstName,
		LastName:  u.LastName,
	}

	return shortU, nil
}

func FindUserByID(db *sql.DB, id int64) (User, error) {
	const findUserByEmailQuery = `SELECT ` + userFields + ` FROM users WHERE id = $1`
	row := db.QueryRow(findUserByEmailQuery, id)

	var u User
	err := scanUser(row, &u)
	if err != nil {
		return User{}, errors.Wrap(err, "can't scan user")
	}

	return u, nil
}

/*func AddUserLot(db *sql.DB, userID int64, lotID int64) error {
	const updateUserQuery = `UPDATE users SET lots = array_append(lots, $1) WHERE id = $2`
	_, err := db.Exec(updateUserQuery, lotID, userID)
	if err != nil {
		return errors.Wrapf(err, "can't add user lot in database: %s", err)
	}

	return nil
}*/
