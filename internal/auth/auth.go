package auth

import (
	"database/sql"

	"github.com/pkg/errors"
	"gitlab.com/maxboychuk/course-project/internal/user"
	"golang.org/x/crypto/bcrypt"
)

func AuthorizationUser(db *sql.DB, email string, pswd string) error {
	regUser, err := user.FindUserByEmail(db, email)
	if err != nil {
		return errors.Wrap(err, "can't find user by email")
	}

	err = bcrypt.CompareHashAndPassword([]byte(regUser.Password), []byte(pswd))
	if err != nil {
		return errors.Wrap(err, "password and hash not equal")
	}

	return nil
}
